defmodule ListTest do
  use ExUnit.Case

  test "++" do
    small_list_num = 10
    large_list_num = 100

    small_list = for x <- 1..small_list_num, do: x
    large_list = for x <- 1..large_list_num, do: x

    Benchee.run(
      %{
        "#{small_list_num} members ++ #{large_list_num} members" => fn ->
          small_list ++ large_list
        end,
        "#{large_list_num} members ++ #{small_list_num} members" => fn ->
          large_list ++ small_list
        end
      },
      time: 10
    )
  end

  test "++2" do
    small_list_num = 0
    large_list_num = 50

    small_list = []
    large_list = for x <- 0..large_list_num, do: x

    Benchee.run(
      %{
        "#{small_list_num} members ++ #{large_list_num} members" => fn ->
          small_list ++ large_list
        end,
        "#{large_list_num} members ++ #{small_list_num} members" => fn ->
          large_list ++ small_list
        end
      },
      time: 10
    )
  end

  test "flatten" do
    small_list_num = 10
    large_list_num = 100

    nest_list1 = for _ <- 1..small_list_num, do: for(x <- 1..large_list_num, do: x)
    nest_list2 = for _ <- 1..large_list_num, do: for(x <- 1..small_list_num, do: x)

    Benchee.run(
      %{
        "#{small_list_num} #{large_list_num} member list flatten" => fn ->
          List.flatten(nest_list1)
        end,
        "#{large_list_num} #{small_list_num} member list flatten" => fn ->
          List.flatten(nest_list2)
        end
      },
      time: 10
    )
  end

  test "keydelete" do
    list_num = 1000
    list = for x <- 1..list_num, do: {x, x}
    key_1 = 1
    key_2 = div(list_num, 2)
    key_3 = list_num

    Benchee.run(
      %{
        "keydelete #{list_num} pos at #{key_1}" => fn ->
          List.keydelete(list, key_1, 0)
        end,
        "keydelete #{list_num} pos at #{key_2}" => fn ->
          List.keydelete(list, key_2, 0)
        end,
        "keydelete #{list_num} pos at #{key_3}" => fn ->
          List.keydelete(list, key_3, 0)
        end,
      },
      time: 10
    )
  end

  test "keyfind" do
    list_num = 1000
    list = for x <- 1..list_num, do: {x, x}
    key_1 = 1
    key_2 = div(list_num, 2)
    key_3 = list_num

    Benchee.run(
      %{
        "keyfind #{list_num} pos at #{key_1}" => fn ->
          List.keyfind(list, key_1, 0)
        end,
        "keyfind #{list_num} pos at #{key_2}" => fn ->
          List.keyfind(list, key_2, 0)
        end,
        "keyfind #{list_num} pos at #{key_3}" => fn ->
          List.keyfind(list, key_3, 0)
        end,
      },
      time: 10
    )
  end

end
