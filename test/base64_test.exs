defmodule ElixirKernelTest do
  use ExUnit.Case

  test "base64 test" do
    data = :crypto.strong_rand_bytes(100 * 1024)
    base64_data = Base.encode64(data)

    Benchee.run(
      %{
        "base64encode" => fn ->
          Base.encode64(data)
        end,
        "base64decode" => fn ->
          Base.decode64!(base64_data)
        end
      },
      time: 20
    )
  end
end
