defmodule UUIDTest do
  use ExUnit.Case

  test "uuid test" do
    Benchee.run(
      %{
        "uuid" => fn ->
          UUID.uuid1()
        end,
      },
      time: 20
    )
  end
end
