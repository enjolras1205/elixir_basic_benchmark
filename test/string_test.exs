defmodule StringTest do
  use ExUnit.Case

  test "++" do
    atom_val = :P10082
    db = "_chat"

    Benchee.run(
      %{
        "string_format" => fn ->
          for _ <- 1..100 do
            "#{atom_val}#{db}"
          end
        end,
        "string_concat" => fn ->
          for _ <- 1..100 do
            Atom.to_string(atom_val) <> db
          end
        end
      },
      time: 10
    )
  end

  defmodule Test do
    require Logger

    def xxx(a, b \\ 1, c \\ 2, d) do
      Logger.warn("a:#{a}, b:#{b} c:#{c} d:#{d}")
    end
  end

  test "" do
    Test.xxx(0, 3)
  end
end
