defmodule ErlangMapEmptyTest do
  require Logger
  use ExUnit.Case
  @loop_count 500

  test "erlang term performance" do
    term = Enum.into(1..100000, %{}, fn v -> {v, v} end)
    Logger.info("map_size:#{map_size(term)}")

    Benchee.run(
      %{
        "map size 0" => fn ->
          for _ <- 1..@loop_count do
            case :ok do
              _ when map_size(term) == 0 ->
                :ok

              _ ->
                :ok
            end
          end
        end,
        "map equal %{}" => fn ->
          for _ <- 1..@loop_count do
            case :ok do
              _ when term == %{} ->
                :ok

              _ ->
                :ok
            end
          end
        end
      },
      time: 10
    )
  end
end
