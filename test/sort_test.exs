defmodule ElixirSortTest do
  use ExUnit.Case

  test "sort test" do
    sort_msgs1 =
      for v <- 1..100 do
        %{msg_id: v, content: %{"hello" => "world"}}
      end

    sort_msgs2 =
      for v <- 100..1 do
        %{msg_id: v, content: %{"hello" => "world"}}
      end

    Benchee.run(
      %{
        "sort msg 1" => fn ->
          for _ <- 1..100 do
            Enum.sort(sort_msgs1, fn a, b -> a.msg_id > b.msg_id end)
          end
        end,
        "sort msg 2" => fn ->
          for _ <- 1..100 do
            Enum.sort(sort_msgs2, fn a, b -> a.msg_id > b.msg_id end)
          end
        end
      },
      time: 10
    )
  end
end
