defmodule ElixirKernelTest do
  use ExUnit.Case

  defmodule Test do
    def xxx(_a, _b) do
    end
  end

  test "kernel test" do
    Benchee.run(
      %{
        "1" => fn ->
          Kernel.function_exported?(ElixirKernelTest, :xxx, 2)
        end,
        "2" => fn ->
          Kernel.function_exported?(Test, :xxx, 2)
        end
      },
      time: 10
    )
  end
end
