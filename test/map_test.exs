defmodule ElixirMapTest do
  use ExUnit.Case

  test "map test" do
    small_map = Enum.into(1..31, %{}, fn x -> {x, x} end)
    large_map = Enum.into(1..10000, %{}, fn x -> {x, x} end)

    small_keys =
      for _ <- 1..50 do
        :rand.uniform(31)
      end ++
        for _ <- 1..50 do
          :rand.uniform(31) + 100
        end

    small_put_keys =
      for _ <- 1..100 do
        :rand.uniform(31)
      end

    large_keys =
      for _ <- 1..50 do
        :rand.uniform(10000)
      end ++
        for _ <- 1..50 do
          :rand.uniform(10000) + 10000
        end

    Benchee.run(
      %{
        "test small get" => fn ->
          for key <- small_keys do
            Map.get(small_map, key)
          end
        end,
        "test small put" => fn ->
          for key <- small_put_keys do
            Map.put(small_map, key, key)
          end
        end,
        "test small merge" => fn ->
          for key <- small_put_keys do
            Map.merge(small_map, %{key: key})
          end
        end,
        "test small delete" => fn ->
          for key <- small_put_keys do
            Map.delete(small_map, key)
          end
        end,
        "test large get" => fn ->
          for key <- large_keys do
            Map.get(large_map, key)
          end
        end,
        "test large put" => fn ->
          for key <- large_keys do
            Map.put(large_map, key, key)
          end
        end,
        "test large merge" => fn ->
          for key <- large_keys do
            Map.merge(large_map, %{key: key})
          end
        end,
        "test large delete" => fn ->
          for key <- large_keys do
            Map.delete(large_map, key)
          end
        end
      },
      time: 10
    )
  end
end
