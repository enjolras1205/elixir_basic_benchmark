defmodule ElixirBasicBenchmarkTest do
  use ExUnit.Case

  test "set and map" do
    ets_a =
      :ets.new(nil, [
        :set,
        :public
      ])

    map = %{}

    test_keys =
      for _ <- 1..5_000 do
        {:rand.uniform(5000), :rand.uniform(5000)}
      end

    tasks = %{
      "test_ets" => fn ->
        for {insert_key, lookup_key} <- test_keys do
          :ets.insert(ets_a, {insert_key, :method, :dest, "123456789012123"})
          :ets.lookup(ets_a, lookup_key)
          :ets.delete(ets_a, lookup_key)
        end
      end,
      "test_dict" => fn ->
        Enum.reduce(test_keys, map, fn {insert_key, lookup_key}, acc ->
          acc = Map.put(acc, insert_key, {insert_key, :method, :dest, "123456789012123"})
          Map.get(acc, lookup_key)
          Map.delete(acc, lookup_key)
        end)
      end
    }

    Benchee.run(
      tasks,
      time: 10,
      parallel: 1
      # memory_time: 2
    )
  end

  test "gb_tree and orderset" do
    ets_a =
      :ets.new(nil, [
        :ordered_set,
        :public
      ])

    gb_tree = :gb_trees.empty()

    test_keys =
      for _ <- 1..5_000 do
        {:rand.uniform(1_000), :rand.uniform(1_000)}
      end

    tasks = %{
      "test_ets" => fn ->
        for {insert_key, lookup_key} <- test_keys do
          :ets.insert(ets_a, {insert_key, insert_key})
          :ets.lookup(ets_a, lookup_key)
          :ets.delete(ets_a, lookup_key)
        end
      end,
      "test_gbt" => fn ->
        Enum.reduce(test_keys, gb_tree, fn {insert_key, lookup_key}, acc ->
          acc = :gb_trees.enter(insert_key, insert_key, acc)
          :gb_trees.lookup(lookup_key, acc)

          try do
            :gb_trees.delete(lookup_key, acc)
          catch
            _, _ ->
              acc
          end
        end)
      end
    }

    Benchee.run(
      tasks,
      time: 10,
      parallel: 1
      # memory_time: 2
    )
  end

  test "ets read" do
    ets_set_with_concurrency = :a
    ets_ordered_set_with_concurrency = :b
    ets_duplicate_bag_with_concurrency = :c
    ets_bag_with_concurrency = :d
    :ets.new(ets_set_with_concurrency, [:set, :protected, :named_table, read_concurrency: true])

    :ets.new(ets_ordered_set_with_concurrency, [
      :ordered_set,
      :protected,
      :named_table,
      read_concurrency: true
    ])

    :ets.new(ets_duplicate_bag_with_concurrency, [
      :duplicate_bag,
      :protected,
      :named_table,
      read_concurrency: true
    ])

    :ets.new(ets_bag_with_concurrency, [:bag, :protected, :named_table, read_concurrency: true])

    ets_set_without_concurrency = :A
    ets_ordered_set_without_concurrency = :B
    ets_duplicate_bag_without_concurrency = :C
    ets_bag_without_concurrency = :D

    :ets.new(ets_set_without_concurrency, [
      :set,
      :protected,
      :named_table,
      read_concurrency: false
    ])

    :ets.new(ets_ordered_set_without_concurrency, [
      :ordered_set,
      :protected,
      :named_table,
      read_concurrency: false
    ])

    :ets.new(ets_duplicate_bag_without_concurrency, [
      :duplicate_bag,
      :protected,
      :named_table,
      read_concurrency: false
    ])

    :ets.new(ets_bag_without_concurrency, [
      :bag,
      :protected,
      :named_table,
      read_concurrency: false
    ])

    Enum.map(1..1_000_000, fn x ->
      value = :crypto.strong_rand_bytes(40)
      :ets.insert(ets_set_with_concurrency, {x, value})
      :ets.insert(ets_ordered_set_with_concurrency, {x, value})
      :ets.insert(ets_duplicate_bag_with_concurrency, {x, value})
      :ets.insert(ets_bag_with_concurrency, {x, value})
      :ets.insert(ets_set_without_concurrency, {x, value})
      :ets.insert(ets_ordered_set_without_concurrency, {x, value})
      :ets.insert(ets_duplicate_bag_without_concurrency, {x, value})
      :ets.insert(ets_bag_without_concurrency, {x, value})
    end)

    test_keys =
      for _ <- 1..100 do
        :rand.uniform(1_000_000)
      end

    tasks = %{
      "ets_set_with_read_concurrency" => fn ->
        Enum.map(test_keys, fn x ->
          :ets.lookup(ets_set_with_concurrency, x)
        end)
      end,
      "ets_ordered_set_with_read_concurrency" => fn ->
        Enum.map(test_keys, fn x ->
          :ets.lookup(ets_ordered_set_with_concurrency, x)
        end)
      end,
      "ets_duplicate_bag_with_read_concurrency" => fn ->
        Enum.map(test_keys, fn x ->
          :ets.lookup(ets_duplicate_bag_with_concurrency, x)
        end)
      end,
      "ets_bag_with_read_concurrency" => fn ->
        Enum.map(test_keys, fn x ->
          :ets.lookup(ets_bag_with_concurrency, x)
        end)
      end,
      "ets_set_without_read_concurrency" => fn ->
        Enum.map(test_keys, fn x ->
          :ets.lookup(ets_set_without_concurrency, x)
        end)
      end,
      "ets_ordered_set_without_read_concurrency" => fn ->
        Enum.map(test_keys, fn x ->
          :ets.lookup(ets_ordered_set_without_concurrency, x)
        end)
      end,
      "ets_duplicate_bag_without_read_concurrency" => fn ->
        Enum.map(test_keys, fn x ->
          :ets.lookup(ets_duplicate_bag_without_concurrency, x)
        end)
      end,
      "ets_bag_without_read_concurrency" => fn ->
        Enum.map(test_keys, fn x ->
          :ets.lookup(ets_bag_without_concurrency, x)
        end)
      end
    }

    Benchee.run(
      tasks,
      time: 10,
      parallel: 1
      # memory_time: 2
    )

    Benchee.run(
      tasks,
      time: 10,
      parallel: 4
      # memory_time: 2
    )
  end
end
