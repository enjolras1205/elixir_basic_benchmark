defmodule ElixirProcessDictTest do
  use ExUnit.Case

  test "process dict" do
    for num <- 1..1_000_000 do
      Process.put(num, :a)
    end

    test_keys =
      for _ <- 1..1000 do
        :rand.uniform(1_000_000)
      end

    Benchee.run(
      %{
        "test get" => fn ->
          for key <- test_keys do
            Process.get(key, :b)
          end
        end,
        "test put" => fn ->
          for key <- test_keys do
            Process.put(key, :b)
          end
        end,
        "test put and delete" => fn ->
          for key <- test_keys do
            Process.put(key, :b)
            Process.delete(key)
          end
        end,
        "test delete" => fn ->
          for key <- test_keys do
            Process.delete(key)
          end
        end
      },
      time: 10
    )
  end
end
