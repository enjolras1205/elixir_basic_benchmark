defmodule HashTest do
  use ExUnit.Case

  test "hash test" do
    Benchee.run(
      %{
        "hash" => fn ->
          nonce = "852631d2abece626"
          username = "t10127_plat"
          password = "aaaaaaaaaaaaaaaaaaaaaaaa"
          hash_password = hash(username <> ":mongo:" <> password)
          key = hash(nonce <> username <> hash_password)
        end
      },
      time: 20
    )
  end

  defp hash(data) do
    :crypto.hash(:md5, data) |> binary_to_hex
  end

  defp binary_to_hex(bin) do
    for <<(<<b::4>> <- bin)>>, into: <<>> do
      <<Integer.to_string(b, 16)::binary>>
    end
    |> String.downcase()
  end
end
