defmodule ReverseTest do
  use ExUnit.Case

  test "base64 test" do
    list = Enum.to_list(1..10000)

    Benchee.run(
      %{
        "reverse1" => fn ->
          Enum.reverse(list) |> Enum.to_list()
        end,
        "reverse10" => fn ->
          Enum.reverse(list)
          |> Enum.reverse()
          |> Enum.reverse()
          |> Enum.reverse()
          |> Enum.reverse(list)
          |> Enum.reverse()
          |> Enum.reverse()
          |> Enum.reverse()
          |> Enum.reverse(list)
          |> Enum.reverse()
          |> Enum.reverse()
          |> Enum.reverse()
          |> Enum.to_list()
        end
      },
      time: 5
    )
  end
end
