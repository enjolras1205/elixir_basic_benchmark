defmodule ErlangTermTest do
  use ExUnit.Case

  test "erlang term performance" do
    bin = :crypto.strong_rand_bytes(100 * 1024)
    list = Enum.map(1..1_000_000, fn v -> v end)
    tuple1 = {:ok, bin}
    tuple2 = {:ok, list}
    term = Enum.map(1..100, fn v -> %{asdf: :qwer, tttttttttt: v, sss: :jjj} end)

    Benchee.run(
      %{
        "binary size" => fn ->
          :erlang_term.byte_size(bin)
        end,
        "list size" => fn ->
          :erlang_term.byte_size(list)
        end,
        "tuple size 1" => fn ->
          :erlang_term.byte_size(tuple1)
        end,
        "tuple size 2" => fn ->
          :erlang_term.byte_size(tuple2)
        end,
        "term size" => fn ->
          :erlang_term.byte_size(term)
        end
      },
      time: 5
    )
  end
end
