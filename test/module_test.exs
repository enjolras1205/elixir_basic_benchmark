defmodule ModuleTest do
  use ExUnit.Case
  require Logger

  test "module concat" do
    Benchee.run(
      %{
        "module concat" => fn ->
          for _ <- 1..100 do
            Module.concat(Cluster.Entity.Master, :user)
          end
        end
      },
      time: 10
    )
  end

  test "random" do
    Benchee.run(
      %{
        "random" => fn ->
          for _ <- 1..100 do
            Enum.random([1, 2, 3, 4])
          end
        end
      },
      time: 10
    )
  end

  test "xxxx" do
    Benchee.run(
      %{
        "random" => fn ->
          val = Enum.take_random(1..100_000_000, 1000_0000)
          len = length(val)
          Logger.info("len:#{len}")
        end
      },
      time: 20
    )
  end
end
