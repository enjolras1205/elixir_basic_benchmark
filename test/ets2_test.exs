defmodule ElixirBasicBenchmarkTest do
  use ExUnit.Case
  require Logger
  @user_num 50000

  test "find in ets" do
    ets_a =
      :ets.new(nil, [
        :ordered_set,
        :public
      ])

    ets_b =
      :ets.new(nil, [
        :ordered_set,
        :public
      ])

    [group_id_a | _] =
      group_ids =
      Enum.map(1..100, fn v ->
        "group_id_qwerty_qwerty_#{v}"
      end)

    user_ids =
      Enum.map(1..@user_num, fn v ->
        "user_id:#{v}"
      end)

    for group_id <- group_ids do
      :ets.insert(ets_a, {{group_id, nil}, false})

      for user_id <- user_ids do
        :ets.insert(ets_a, {{group_id, user_id}, true})
      end
    end

    for user_id <- user_ids do
      :ets.insert(ets_b, {user_id, true})
    end

    for group_id <- group_ids do
      result = find_all_user_ids(ets_a, group_id)

      assert length(result) == @user_num
    end

    tasks = %{
      "test_ets" => fn ->
        find_all_user_ids(ets_a, group_id_a)
      end,
      "test_ets2" => fn ->
        b = :ets.tab2list(ets_b)
      end
    }

    Benchee.run(
      tasks,
      time: 10,
      parallel: 1
      # memory_time: 2
    )
  end

  def find_all_user_ids(ets_name, group_id) do
    next_key = :ets.next(ets_name, {group_id, nil})
    find_xx(ets_name, next_key, group_id, [])
  end

  def find_xx(ets_name, {group_id1, user_id} = cur_key, group_id2, result)
      when group_id1 == group_id2 do
    next_key = :ets.next(ets_name, cur_key)
    find_xx(ets_name, next_key, group_id2, [user_id | result])
  end

  def find_xx(_, _, _, result) do
    result
  end
end
