defmodule ElixirBasicBenchmarkLogMetaTest do
  use ExUnit.Case
  require Logger

  test "meta" do
    Benchee.run(
      %{
        "meta" => fn ->
          for _ <- 1..100 do
            Logger.metadata(trace: "aa")
          end
        end
      },
      time: 10,
    )
  end
end
