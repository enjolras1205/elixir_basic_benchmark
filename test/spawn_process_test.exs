defmodule SpawnProcessTest do
  use ExUnit.Case

  test "spawn" do
    Benchee.run(
      %{
        "spawn" => fn ->
          Enum.map(1..100, fn x ->
            Task.start(fn -> :ok end)
          end)
        end
      },
      time: 5,
      memory_time: 2
    )
  end
end
