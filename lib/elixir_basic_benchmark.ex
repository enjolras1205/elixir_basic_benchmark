defmodule ElixirBasicBenchmark do
  @moduledoc """
  Documentation for ElixirBasicBenchmark.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ElixirBasicBenchmark.hello()
      :world

  """
  require Logger

  def new_map(map_num, map_size) do
    mem_info = :erlang.memory()
    Logger.info("before memory #{inspect(mem_info)}")

    res =
      for _val <- 1..map_num do
        :maps.from_list(for x <- 1..map_size, do: {x, x})
      end

    mem_info = :erlang.memory()
    Logger.info("after memory #{inspect(mem_info)}")
  end


  def test() do
    a = %{a: :b}
    Enum.map([%{key: :a}, %{key: :b}], &test2(&1, Map.get(a, &1.key)))
  end

  defp test2(a, b) do
    Logger.warn("a:#{inspect(a)} b:#{b}")
  end
end
