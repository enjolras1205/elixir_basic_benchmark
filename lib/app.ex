defmodule App do
  require Logger
  use Application

  def start(type, args) do
    Logger.info("start1 type:#{inspect(type)} args:#{inspect(args)}")
    Logger.info("start2 type:#{inspect(type)} args:#{inspect(args)}")

    opts = [
      strategy: :one_for_one,
      name: __MODULE__
    ]

    Supervisor.start_link([], opts)
  end
end
